class PodcastsController < ApplicationController
  before_action :set_podcast, only: [:show]

  def index
    params[:page]    ||= {}
    ransack_params     = params[:filter] || {}
    ransack_params[:s] = sorted_fields(params[:sort], ["created_at desc"])

    @podcasts = Podcast.ransack(ransack_params).result
      .page(params[:page][:number])
      .per(params[:page][:size])

    render json: @podcasts, include: params[:include]
  end

  def show
    render json: @podcast, include: params[:include]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_podcast
      @podcast = Podcast
        .find(params[:id])
    end
end
