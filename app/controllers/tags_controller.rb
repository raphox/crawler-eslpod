class TagsController < ApplicationController
  before_action :set_tag_collection, only: [:index, :show]
  before_action :set_tag, only: [:show]

  def index
    params[:page] ||= {}

    render json: @tags
      .order(name: :asc)
      .page(params[:page][:number])
      .per(params[:page][:size])
  end
  
  def show
    render json: @tag
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tag_collection
      @tags = Tag
        
      unless params[:podcast_id].blank?
        @tags = @tags.includes(:podcasts).where(podcasts: { id: params[:podcast_id] })
      end
    end

    def set_tag
      @tag = @tags
        .find(params[:id])
    end
end
