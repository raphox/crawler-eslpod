module SortParams
  extend ActiveSupport::Concern

  def sorted_fields(sort, default = {})
    fields = sort.to_s.split(',')

    ordered_fields = convert_to_ordered_hash(fields)

    ordered_fields.present? ? ordered_fields : default
  end

  def convert_to_ordered_hash(fields)
    fields.map do |field|
      if field.start_with?('-')
        field[1..-1] + " desc"
      else
        field + " asc"
      end
    end
  end
end