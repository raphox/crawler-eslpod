class PodcastSerializer < ActiveModel::Serializer
  attributes :id,
    :title,
    :category,
    :number,
    :description,
    :path,
    :created_at

  has_many :tags do
    link :related do
      href podcast_tags_url(object)
      meta count: object.tags.length
    end
  end

  link(:self) { podcast_url(object) }
end
