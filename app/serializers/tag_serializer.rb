class TagSerializer < ActiveModel::Serializer
  attributes :id, :name, :created_at
  
  link(:self) { tag_url(object) }
end
