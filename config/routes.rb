Rails.application.routes.draw do
  resources :podcasts, only: [:index, :show] do
    resources :tags, only: [:index, :show]
  end

  resources :tags, only: [:index, :show]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
