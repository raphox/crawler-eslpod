class CreateTags < ActiveRecord::Migration[5.0]
  def change
    create_table :tags do |t|
      t.string :name

      t.timestamps
    end

    create_join_table :podcasts, :tags do |t|
      t.index :podcast_id
      t.index :tag_id
    end
    
    add_index :podcasts_tags, [:podcast_id, :tag_id], :unique => true
  end
end
