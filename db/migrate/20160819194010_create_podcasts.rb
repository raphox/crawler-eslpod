class CreatePodcasts < ActiveRecord::Migration[5.0]
  def change
    create_table :podcasts do |t|
      t.string :title
      t.string :category
      t.integer :number
      t.text :description
      t.text :path

      t.timestamps
    end
  end
end
