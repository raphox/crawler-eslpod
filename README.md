# Run

## API

```
cd .
rails s -b $IP -p $PORT
```

## UI

https://bitbucket.org/raphox/crawler-eslpod-ui

## Crawler

```
rails eslpdo:collect # Collect data from https://www.eslpod.com/website/show_all.php
rails eslpdo:csv # Generate CSV with all podcasts in database
```

# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
