require 'csv'

namespace :eslpdo do
  desc "Generate CSV with all registers"
  task :csv => :environment do
    file = Rails.root.join('tmp', 'podcasts', "#{Time.now.strftime('%Y%m%d%H%M%S')}.csv")
    path = File.dirname(file)

    FileUtils.mkdir_p(path) unless File.directory?(path)

    CSV.open(file, 'w', { :col_sep => "\t" }) do |csv|
      Podcast.all.each do |podcast|
        csv << podcast.attributes.values_at(*Podcast.column_names)
      end
    end
  end

  desc "This task does nothing"
  task :collect => :environment do
    (0..5).step(20) do |index|
      parameters = { low_rec: index } # last is 1800

      uri       = URI.parse("https://www.eslpod.com/website/show_all.php")
      uri.query = URI.encode_www_form(parameters)

      res  = Rails.cache.fetch(uri, expires_in: 12.hours) { Net::HTTP.get_response(uri) }
      html = Nokogiri::HTML(res.body).at_xpath('/html/body/table[2]/tr/td[3]/table/tr[2]/td[2]')

      html.xpath('//table[contains(@class, "podcast_table_home")]').each do |item|
        title, description = nil

        date = item.at_xpath('.//span[contains(@class, "date-header")]').text.split(/–|-/).last.squish rescue nil

        next unless date

        header = item.at_xpath('.//tr/td/span[contains(@class, "pod_body")]/strong').text.split(/–|-/)
        title  = header.length > 1 ? header.pop : nil
        header = header.first.split(' ')

        podcast = Podcast.find_or_initialize_by({
          number:   header.pop.squish,
          category: header.join(' ').squish
        })

        description = item.at_xpath('.//tr/td/span[contains(@class, "pod_body")]/span[contains(@class, "pod_body")]')

        tags = description.xpath('.//a').last.text rescue nil
        path = description.at_xpath('.//a[text()="Download Podcast"]')["href"] rescue nil

        description.xpath('.//span').remove
        description.xpath('.//a').remove

        description = description.text.gsub('Tags:', '').squish.gsub(/,+$/, '').squish

        unless tags.blank?
          tags.split(',').each do |_tag|
            tag = Tag.find_or_create_by(name: _tag.squish)

            podcast.tags << tag unless podcast.tags.exists?(tag.id)
          end
        end

        podcast.title       = title.blank?       ? nil : title.squish
        podcast.description = description.blank? ? nil : description.squish
        podcast.path        = path.blank?        ? nil : path.squish
        podcast.created_at  = date.to_date

        podcast.save
      end

      # sleep 10
    end
  end
end
